#include "Pawn.h"

Pawn::Pawn(const char name) : ChessPiece(name)
{
}

Pawn::~Pawn()
{
}

int Pawn::isValidMove(const int* src, const int* dst) const
{
	if (abs(dst[0] - src[0]) > 2 || abs(dst[1] - src[1]) > 1) {
		return ILLEGAL_MOVE;
	}
	if (abs(dst[0] - src[0]) == 2 && abs(dst[1] - src[1]) == 0) {// if want to move 2 steps
		if (this->getName() == 'p' && src[0] != 1) { //white has alredy moved
			return ILLEGAL_MOVE;
		}
		else if (this->getName() == 'P' && src[0] != 6) { //black has alredy moved
			return ILLEGAL_MOVE;
		}
	}
	else if (this->getName() == 'p' && dst[0] - src[0] != 1) {
		return ILLEGAL_MOVE;
	}
	else if (this->getName() == 'P' && dst[0] - src[0] != -1) {
		return ILLEGAL_MOVE;
	}
	return VALID_MOVE;
}

int Pawn::isValidEat(const int* src, const int* dst, const char dstKind) const
{
	if (dstKind != EMPTY)
	{
		if (abs(src[0] - dst[0]) != 1 || abs(src[1] - dst[1]) != 1) { // if eat someone in front of him
			return ILLEGAL_MOVE;
		}
		
	}
	if (dstKind == EMPTY){
		if ((abs(src[0] - dst[0]) != 1 && abs(src[0] - dst[0]) != 2) || abs(src[1] - dst[1]) != 0) {// if try to eat no one in diagonally
			return ILLEGAL_MOVE;
		}
	}
	return VALID_MOVE;
}