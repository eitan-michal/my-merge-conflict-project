#pragma once
#include "ChessPiece.h"

class Empty : public ChessPiece
{
public:
	Empty(const char name);
	~Empty();

	int isValidMove(const int* src, const int* dst) const;
};
