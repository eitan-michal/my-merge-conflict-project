#pragma once
#include <iostream>
#include "ChessPiece.h"
#include "Rook.h"
#include "King.h"
#include "Bishop.h"
#include "Queen.h"
#include "Knight.h"
#include "Pawn.h"
#include "Empty.h"
#include <vector>

#define BOARD_SIZE 8
#define NO_CHECK -1
#define LOC 2

#define LAST_ROW 7

class Board
{
public:
	Board(const std::string str);
	~Board();
	Board(const Board& other);

	void printBoard() const;
	int movePiece(const int* src, const int* dst); // moving a piece on the board
	int isCheckMate() const; // if checmate happened

	Board& operator=(const Board& other);

private:
	// Methods:
	int generalValidMove(const int* src, const int* dst) const; // checking if the move is generally legal (not for a specific player)
	int isCheck(); // if a check happened
	void updateKingLoc(const int* newLoc); // updating locations of white / black kings
	bool checkRoute(const int* src, const int* dst) const; // checking if there is someone in the route to dst
	int checkPawnEat(const int* src, const int* dst) const; // checking if pawn's eat is legal

	// Data:
	ChessPiece* _board[BOARD_SIZE][BOARD_SIZE];
	int _whiteKing[LOC];
	int _blackKing[LOC];
	bool _player;// White - true, Black - false
};
