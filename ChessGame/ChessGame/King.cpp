#include "King.h"

King::King(const char name) : ChessPiece(name)
{
}

King::~King()
{
}

int King::isValidMove(const int* src, const int* dst) const
{
	// checking if the king moved only 1 square
	if ((dst[0] - src[0] > 1 || dst[0] - src[0] < -1) || (dst[1] - src[1] > 1 || dst[1] - src[1] < -1))
	{
		return ILLEGAL_MOVE;
	}
	return VALID_MOVE; //if got to here then the move is valid
}
