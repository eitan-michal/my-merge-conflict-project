#include "Queen.h"

Queen::Queen(const char name) : ChessPiece(name)
{
}

Queen::~Queen()
{
}

int Queen::isValidMove(const int* src, const int* dst) const
{
	Bishop b('b');
	Rook r('r');
	if (r.isValidMove(src, dst) != VALID_MOVE && b.isValidMove(src, dst) != VALID_MOVE)
	{
		return ILLEGAL_MOVE;
	}

	return VALID_MOVE;
}
