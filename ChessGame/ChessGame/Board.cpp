#include "Board.h"
#include "Board.h"

Board::Board(const std::string str) : _player(true)
{
	int strIndex = 0;
	_whiteKing[0] = 0;
	_whiteKing[1] = 0;
	_blackKing[0] = 0;
	_blackKing[1] = 0;
	ChessPiece* p;
	for (int i = 0; i < BOARD_SIZE; i++) // initializing a new board by the values in the string
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{	// creating a new player for every character
			if (str[strIndex] == 'r' || str[strIndex] == 'R'){
				p = new Rook(str[strIndex]);
			}
			else if (str[strIndex] == 'k' || str[strIndex] == 'K')
			{
				p = new King(str[strIndex]);
				if (str[strIndex] == 'k') {
					_whiteKing[0] = i;
					_whiteKing[1] = j;
				}
				else{
					_blackKing[0] = i;
					_blackKing[1] = j;
				}
			}
			else if (str[strIndex] == 'b' || str[strIndex] == 'B')
			{
				p = new Bishop(str[strIndex]);
			}
			else if (str[strIndex] == 'q' || str[strIndex] == 'Q')
			{
				p = new Queen(str[strIndex]);
			}
			else if (str[strIndex] == 'n' || str[strIndex] == 'N')
			{
				p = new Knight(str[strIndex]);
			}
			else if (str[strIndex] == 'p' || str[strIndex] == 'P')
			{
				p = new Pawn(str[strIndex]);
			}
			else
			{
				p = new Empty('#');
			}
			_board[i][j] = p;
			strIndex++;
		}
	}
}

Board::~Board()
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (_board[i][j] != NULL)
			{
				delete _board[i][j];
			}
		}
	}
}

Board::Board(const Board& other)
{
	_board[0][0] = nullptr;
	*this = other; // uses copy operator - copy other object to this object
}

Board& Board::operator=(const Board& other){
	if (this == &other) {
		return *this;
	}

	bool emptyBoard = (_board[0][0] == nullptr);
	this->_player = other._player;
	_whiteKing[0] = other._whiteKing[0];
	_whiteKing[1] = other._whiteKing[1];
	_blackKing[0] = other._blackKing[0];
	_blackKing[1] = other._blackKing[1];

	ChessPiece* temp;
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{	// copying board - allocating new memory for each player
			if (other._board[i][j]->getName() == 'r' || other._board[i][j]->getName() == 'R') {
				temp = new Rook(other._board[i][j]->getName());
			}
			else if (other._board[i][j]->getName() == 'k' || other._board[i][j]->getName() == 'K')
			{
				temp = new King(other._board[i][j]->getName());
			}
			else if (other._board[i][j]->getName() == 'b' || other._board[i][j]->getName() == 'B')
			{
				temp = new Bishop(other._board[i][j]->getName());
			}
			else if (other._board[i][j]->getName() == 'q' || other._board[i][j]->getName() == 'Q')
			{
				temp = new Queen(other._board[i][j]->getName());
			}
			else if (other._board[i][j]->getName() == 'n' || other._board[i][j]->getName() == 'N')
			{
				temp = new Knight(other._board[i][j]->getName());
			}
			else if (other._board[i][j]->getName() == 'p' || other._board[i][j]->getName() == 'P')
			{
				temp = new Pawn(other._board[i][j]->getName());
			}
			else
			{
				temp = new Empty('#');
			}
			if (!emptyBoard) // if memory was allocated
			{
				delete this->_board[i][j]; // deleting previous memory
			}
			this->_board[i][j] = temp; // replacing with new player
		}
	}
	return *this;
}

void Board::printBoard() const
{
	for (int i = BOARD_SIZE - 1; i >= 0; i--)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			std::cout << _board[i][j]->getName() << " ";
		}
		std::cout << std::endl;
	}
}

int Board::movePiece(const int* src, const int* dst)
{
	int validMove = 0;
	// checking if move is valid
	if ((validMove = this->generalValidMove(src, dst)) || (validMove = this->_board[src[0]][src[1]]->isValidMove(src, dst)) || (validMove = this->checkPawnEat(src, dst))) {
		return validMove;
	}

	Board newBoard(*this); // saving current board (in case we have to restore it)

	// moving piece on the board
	Empty* p = new Empty('#');
	delete _board[dst[0]][dst[1]];
	_board[dst[0]][dst[1]] = _board[src[0]][src[1]];
	_board[src[0]][src[1]] = p;

	// updating king's location
	updateKingLoc(dst);

	int check = this->isCheck(); // checking if there was a check
	if (check == SELF_CHECK) { // if player did a check ob himself
		*this = newBoard;
		updateKingLoc(src);
	}
	else {
		_player = !_player; //change player's turn
	}

	return check; 
}

int Board::generalValidMove(const int* src, const int* dst) const
{
	// if the src/dst have illegal values
	if ((src[0] < 0 || src[0] >= BOARD_SIZE) || (src[1] < 0 || src[1] >= BOARD_SIZE) || (dst[0] < 0 || dst[0] >= BOARD_SIZE) || (dst[1] < 0 || dst[1] >= BOARD_SIZE)) {
		return ILLEGAL_INDEX;
	}
	if (src[0] == dst[0] && src[1] == dst[1]) { //if the same place
		return SAME_LOC;
	}
	// if the src is empty / it's not the current player's turn
	if (_board[src[0]][src[1]]->getName() == EMPTY || bool(isupper(_board[src[0]][src[1]]->getName())) == _player) {
		return NOT_THE_PLAYER;
	}
	if (_board[dst[0]][dst[1]]->getName() != EMPTY) {// if dest empty then no need to chack
		if (isupper(_board[src[0]][src[1]]->getName()) == isupper(_board[dst[0]][dst[1]]->getName())) {// if src and dst are the same team
			return SAME_PLAYER_DST;
		}
	}
	if (checkRoute(src, dst) == false) {
		return ILLEGAL_MOVE;
	}

	return VALID_MOVE;
}

int Board::isCheck()
{
	bool route = false;
	int validMove = 0;
	int curSrc[LOC] = { 0 };
	Pawn p('p');
	// checking each player if he can move to the enemy's king location
	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {

			curSrc[0] = i;
			curSrc[1] = j;
			if (isupper(_board[i][j]->getName()))// if current player is black
			{
				validMove = _board[i][j]->isValidMove(curSrc, _whiteKing); // checking if he can move to the white king's location
				route = checkRoute(curSrc, _whiteKing);
				if (route != false && validMove == VALID_MOVE && checkPawnEat(curSrc, _whiteKing) == VALID_MOVE) {
					if (_player == false) // the check if valid
					{
						return VALID_CHECK_MOV;
					}
					else{ // self check of the white player - not valid
						return SELF_CHECK;
					}
				}
			}
			else if (_board[i][j]->getName() != EMPTY)// if current player is  white
			{
				validMove = _board[i][j]->isValidMove(curSrc, _blackKing); // checking if he can move to the black king's location
				route = checkRoute(curSrc, _blackKing);
				if (route != false && validMove == VALID_MOVE && checkPawnEat(curSrc, _blackKing) == VALID_MOVE) {
					if (_player == true) // the check if valid
					{
						return VALID_CHECK_MOV;
					}
					else { // self check of the white player - not valid
						return SELF_CHECK;
					}
				}
			}
		}
	}
	return VALID_MOVE;

}

void Board::updateKingLoc(const int* newLoc)
{
	if (_board[newLoc[0]][newLoc[1]]->getName() == 'k') {
		_whiteKing[0] = newLoc[0];
		_whiteKing[1] = newLoc[1];
	}
	else if (_board[newLoc[0]][newLoc[1]]->getName() == 'K') {
		_blackKing[0] = newLoc[0];
		_blackKing[1] = newLoc[1];
	}
}

bool Board::checkRoute(const int* src, const int* dst) const
{
	if (_board[src[0]][src[1]]->getName() == 'n' || _board[src[0]][src[1]]->getName() == 'N') { // knight can skip pieces
		return true;
	}

	int row = src[0], col = src[1], colProg = 0, rowProg = 0;
	// checking the direction we go (up / down)
	if (row < dst[0]) {
		rowProg++;
	}
	else if (row > dst[0]){
		rowProg--;
	}
	if (col < dst[1]) {
		colProg++;
	}
	else if (col > dst[1]) {
		colProg--;
	}

	(row != dst[0]) ? row += rowProg : row; // moving once
	(col != dst[1]) ? col += colProg : col;

	while (row != dst[0] || col != dst[1])
	{
		if (_board[row][col]->getName() != EMPTY)
		{
			return false;
		}
		// checking next step
		(row != dst[0]) ? row += rowProg : row;
		(col != dst[1]) ? col += colProg : col;
	}

	return true;
}

int Board::checkPawnEat(const int* src, const int* dst) const
{
	int validMove = VALID_MOVE;
	if (_board[src[0]][src[1]]->getName() == 'p' || _board[src[0]][src[1]]->getName() == 'P') {// if pawn
		Pawn p('p');
		validMove = p.isValidEat(src, dst, _board[dst[0]][dst[1]]->getName());
	}
	return validMove;
}

int Board::isCheckMate() const
{
	bool isCheck = false, canMove = false;
	int validMove = 0;
	int dst[LOC];
	int king[LOC];
	Board b(*this);

	if (_player == true) { // getting king's location
		king[0] = _whiteKing[0];
		king[1] = _whiteKing[1];
	}
	else {
		king[0] = _blackKing[0];
		king[1] = _blackKing[1];
	}

	if (b.isCheck() == SELF_CHECK) { // first if check occured
		for (int i = -1; i <= 1; i++) // checking each direction the king can go to
		{
			for (int j = -1; j <= 1; j++)
			{
				dst[0] = king[0] + i;
				dst[1] = king[1] + j;
				validMove = b.movePiece(king, dst);
				if (validMove == SELF_CHECK) { // if check happened
					isCheck = true;
				}
				else if (validMove == VALID_MOVE) // if the king can escape
				{
					canMove = true;
				}
				b = *this;
			}
		}
	}
	if (isCheck == true && canMove == false) { // checkmate occur's when there is a check and the king can't escape
		return CHECKMATE;
	}
	return VALID_MOVE;
}
