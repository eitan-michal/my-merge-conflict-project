#include "Empty.h"

Empty::Empty(const char name) : ChessPiece(name)
{
}

Empty::~Empty()
{
}

int Empty::isValidMove(const int* src, const int* dst) const
{
	return NOT_THE_PLAYER; // cant move empty player
}
