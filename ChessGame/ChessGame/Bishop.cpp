#include "Bishop.h"

Bishop::Bishop(const char name) : ChessPiece(name)
{
}

Bishop::~Bishop()
{
}

int Bishop::isValidMove(const int* src, const int* dst) const
{
    if (abs(dst[0] - src[0]) != abs(dst[1] - src[1]))
    {
        return ILLEGAL_MOVE;
    }

    return VALID_MOVE;
}
