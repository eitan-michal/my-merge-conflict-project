#include "Knight.h"

Knight::Knight(const char name) : ChessPiece(name)
{
}

Knight::~Knight()
{
}

int Knight::isValidMove(const int* src, const int* dst) const
{
	if (!((abs(dst[0] - src[0]) == 2 && abs(dst[1] - src[1]) == 1) || (abs(dst[0] - src[0]) == 1 && abs(dst[1] - src[1]) == 2)))
	{
		return ILLEGAL_MOVE;
	}

	return VALID_MOVE;
}
