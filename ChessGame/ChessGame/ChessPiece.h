#pragma once
#include <iostream>
#include <math.h>

using std::abs;

#define EMPTY '#'

#define VALID_MOVE 0
#define VALID_CHECK_MOV 1
#define NOT_THE_PLAYER 2
#define SAME_PLAYER_DST 3
#define SELF_CHECK 4
#define ILLEGAL_INDEX 5
#define ILLEGAL_MOVE 6
#define SAME_LOC 7
#define CHECKMATE 8

class ChessPiece
{
public:
	ChessPiece(const char name);
	~ChessPiece();
	virtual int isValidMove(const int* src, const int* dst) const = 0; // checking each player's move
	char getName() const;
private:
	char _name;
};
