#include "ChessPiece.h"

ChessPiece::ChessPiece(char name)
{
	this->_name = name;
}

ChessPiece::~ChessPiece()
{
	this->_name = 0;
}

char ChessPiece::getName() const
{
	return this->_name;
}
