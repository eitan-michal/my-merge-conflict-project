#include "Rook.h"

Rook::Rook(const char name) : ChessPiece(name)
{
}

Rook::~Rook()
{
}

int Rook::isValidMove(const int* src, const int* dst) const
{
		if (dst[0] != src[0] && dst[1] != src[1]) { // if move is incorect
			return ILLEGAL_MOVE;
		}
		return VALID_MOVE; //if got to here then the move is valid 
}
