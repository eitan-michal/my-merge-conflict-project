#pragma once
#include "ChessPiece.h"
#include "Board.h"
class Pawn : public ChessPiece
{
public:
	Pawn(const char name);
	~Pawn();

	int isValidMove(const int* src, const int* dst) const;
	int isValidEat(const int* src, const int* dst, const char dstKind) const;
};
