#pragma once
#include "ChessPiece.h"

class Rook : public ChessPiece
{
public:
	Rook(const char name);
	~Rook();

	int isValidMove(const int* src, const int* dst) const;
};
