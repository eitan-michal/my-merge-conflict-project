#pragma once
#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;

#define BOARD_STR "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"

class Game
{
public:
	Game();
	~Game();

	void mainMain() const;
};
