#pragma once
#include "ChessPiece.h"
#include "Bishop.h"
#include "Rook.h"

class Queen : public ChessPiece
{
public:
	Queen(const char name);
	~Queen();

	int isValidMove(const int* src, const int* dst) const;
};
