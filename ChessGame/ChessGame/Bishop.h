#pragma once
#include "ChessPiece.h"

class Bishop : public ChessPiece
{
public:
	Bishop(const char name);
	~Bishop();

	int isValidMove(const int* src, const int* dst) const;
};
