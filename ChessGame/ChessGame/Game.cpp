#include "Game.h"

Game::Game()
{
}

Game::~Game()
{
}

void Game::mainMain() const
{
	int src[2];
	int dst[2];
	int validMove = 0;
	int isCheckMate = 0;
	srand(time_t(NULL));

	Board b(BOARD_STR);
	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	
	b.printBoard();

	strcpy_s(msgToGraphics, BOARD_STR); // just example...

	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	strcpy_s(msgToGraphics, "");
	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		src[1] = msgFromGraphics[0] - 'a';
		src[0] = msgFromGraphics[1] - '0' - 1;
		dst[1] = msgFromGraphics[2] - 'a';
		dst[0] = msgFromGraphics[3] - '0' - 1;

		validMove = b.movePiece(src, dst);
		isCheckMate = b.isCheckMate();
		validMove = (isCheckMate == CHECKMATE) ? isCheckMate : validMove;
		msgToGraphics[0] = (char)('0' + validMove);
		msgToGraphics[1] = 0;

		b.printBoard();

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}
