#pragma once
#include "ChessPiece.h"

class Knight : public ChessPiece
{
public:
	Knight(const char name);
	~Knight();

	int isValidMove(const int* src, const int* dst) const;
};
