#pragma once
#include "ChessPiece.h"

class King : public ChessPiece
{
public:
	King(const char name);
	~King();
	
	int isValidMove(const int* src, const int* dst) const;
};
